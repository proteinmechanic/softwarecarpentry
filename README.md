# Software Carpentry: Lesson Material

* Download python-novice-inflammation-data.zip and python-novice-inflammation-code.zip.
* Create a folder called **swc-python** on your Desktop.
* Move downloaded files into this newly created folder.
* Unzip the files.

You should now see two new folders called **data** and **code** in your **swc-python** directory on your Desktop.

